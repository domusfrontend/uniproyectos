<?php

ini_set("display_errors", 1);
ini_set("display_startup_errors", 1);
error_reporting(E_ALL);

require "vendor/autoload.php";

foreach (glob(__DIR__ . "/app/*.php") as $filename) {
    include_once $filename;
}

use App\DetallePageController;
use App\IndexPageController;
use App\ListaPageController;

$index = new IndexPageController();
$lista = new ListaPageController();
$detalle = new DetallePageController();

// Index y subpáginas
Flight::route("/", [$index, "index"]);

// Página de error 404
Flight::map("notFound", [$index, "error404"]);

// Búsqueda de inmuebles
Flight::route("/busqueda/*", [$lista, "index"]);
Flight::route("/mapa", [$index, "mapa"]);

// Detalle del Inmueble
Flight::route("/inmuebles/@name/@id/@codigo", function ($name, $id, $codigo) use ($detalle, $index) {
    if (is_numeric($codigo)) {
        echo $detalle->index($id, $codigo);
    } else {
        echo $index->error404();
    }
});

// Filtro para el mapa
Flight::route("/filtro/mapa/@cantidad/@filtros", function ($cantidad, $filtros) use ($index) {
    echo $index->inmueblesMapa($cantidad, $filtros);
});

// Detalle para cada inmueble en el mapa
Flight::route("/filtro-detalle/mapa/@codigo/@id", function ($codigo, $id) use ($index) {
    echo $index->detalle($codigo, $id, 1);
});

// Limpiar caché
Flight::route("/clear-cache", [$index, "clearCache"]);

Flight::route("POST /svr/form", [$index, "form"]);

Flight::start();
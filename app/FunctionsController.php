<?php

namespace app;

// Paquetes
use Flight;
use PHPMailer\PHPMailer\PHPMailer;

class FunctionsController {

    public function replacespacios($string) {
        $characters = array(
            "Á" => "A", "Ç" => "c", "É" => "e", "Í" => "i", "Ñ" => "n", "Ó" => "o", "Ú" => "u",
            "á" => "a", "ç" => "c", "é" => "e", "í" => "i", "ñ" => "n", "ó" => "o", "ú" => "u",
            "à" => "a", "è" => "e", "ì" => "i", "ò" => "o", "ù" => "u",
        );

        $string = strtr($string, $characters);
        $string = strtolower(trim($string));
        $string = preg_replace("/[^a-z0-9-]/", "-", $string);
        $string = preg_replace("/-+/", "-", $string);

        if (substr($string, strlen($string) - 1, strlen($string)) === "-") {
            $string = substr($string, 0, strlen($string) - 1);
        }

        return str_replace("/", "-", str_replace(" ", "-", strtolower(trim($string, " "))));
    }

    public function cletter($string) {
        return ucwords(mb_strtolower(trim($string, " ")));
    }

    public function toLowerCase($string) {
        return mb_strtolower(trim($string, " "));
    }

    public function priceFormat($string) {
        return "$ " . number_format($string, 0, 2, ".");
    }

    public function pluralizeWord($input, $total, $toLower = false) {
        $string = $this->toLowerCase($input);

        if ($total == 1) {
            $returnString = $string;
        } else {
            $returnString = $string . "s";

            $stringEspeciales = [
                "casa campestre" => "Casas campestres",
                "casa-local" => "Casas-local",
                "casa condominio" => "Casas condominio",
                "inmuebles" => "Inmuebles",
            ];

            $caracteresEspeciales = [
                "l" => "es",
            ];

            foreach ($caracteresEspeciales as $key => $value) {
                if (str_ends_with($string, $key)) {
                    $returnString = $string . $value;
                }
            }

            foreach ($stringEspeciales as $key => $value) {
                if ($string == $key) {
                    $returnString = $value;
                }
            }
        }

        if ($toLower) {
            return $this->toLowerCase($returnString);
        } else {
            return $this->cletter($returnString);
        }
    }

    public function replace_string_in_file($filename, $string_to_replace, $replace_with) {
        $content = file_get_contents($filename);
        $content_chunks = explode($string_to_replace, $content);
        $content = implode($replace_with, $content_chunks);
        file_put_contents($filename, $content);
    }

    public function colors() {

        $content = file_get_contents("assets/css/color.css");
        $color1 = explode(";", explode("--main-bg-color: ", $content)[1]);
        $color2 = explode(";", explode("--second-bg-color: ", $content)[1]);

        return array($color1[0], $color2[0]);
    }

    public function flatten($array, $prefix = "") {
        //Esta función aplana el array
        $result = array();
        foreach ($array as $key => $value) {
            if (is_array($value)) {
                $result = $result + $this->flatten($value, $prefix . $key . ".");
            } else {
                $result[$key] = $value;
            }
        }
        return $result;
    }

    public function getRequestURI($complete = true) {
        $url = "";
        if (isset($_SERVER["HTTPS"]) && $_SERVER["HTTPS"] === "on") {
            $url .= "https";
        } else {
            $url .= "http";
        }

        $url .= "://$_SERVER[HTTP_HOST]";

        if ($complete) {
            $url .= explode("?", $_SERVER["REQUEST_URI"])[0];
        }

        return $url;
    }

    public function getRequestIP() {
        return $_SERVER["REMOTE_ADDR"];
    }

    public function clearCache() {
        if (isset($_SESSION)) {
            session_destroy();
            unset($_SESSION);

            echo "Caché borrada";
            return Flight::redirect("/");
        } else {
            echo "Sin caché por borrar";
            return Flight::redirect("/");
        }
    }

    public function generic_form($data, $files) {
        $request = $data;

        $mailsettings = [
            "host" => "smtp.gmail.com",
            "port" => "587",
            "fromusername" => "cuenta@gmail.com",
            "username" => "cuenta@gmail.com",
            "password" => "contraseñaCuenta",
        ];

        $name = strip_tags(isset($request["name"]) ? $request["name"] : "No especificado", "");
        $email = strip_tags(isset($request["email"]) ? $request["email"] : "No especificado", "");
        $phone = strip_tags(isset($request["phone"]) ? $request["phone"] : "No especificado", "");
        $messages = strip_tags(isset($request["message"]) ? $request["message"] : "No especificado", "");
        $tipo = strip_tags(isset($request["category"]) ? $request["category"] : "No especificado", "");
        $remoteip = strip_tags(isset($request["remoteip"]) ? $request["remoteip"] : "No especificado", "");
        $remoteurl = strip_tags(isset($request["remoteurl"]) ? $request["remoteurl"] : "No especificado", "");
        $fecha = date("m-d-Y");

        $destinatario = "comercial@uniproyectos.com.co";
        //$destinatario = "desarrolloweb@domus.la";
        $destinatario2 = "";
        $destinatario3 = "";
        $destinatario4 = "";
        $nameFriend = $name;

        if ($tipo == 1) {
            //Formulario de contacto genérico
            $asunto = "Contacto desde Pagina web";
            $subjects = "Mensaje enviado desde contaco de Pagina Web";

            $message = file_get_contents("views/mail-templates/detalle_send.html");
            $message = str_replace("%asunto%", $asunto, $message);
            $message = str_replace("%fecha%", $fecha, $message);
            $message = str_replace("%name%", $name, $message);
            $message = str_replace("%email%", $email, $message);
            $message = str_replace("%phone%", $phone, $message);
            $message = str_replace("%subjects%", $subjects, $message);
            $message = str_replace("%message%", $messages, $message);
        } else if ($tipo == 2) {
            //Formulario desde detalle del inmueble
            $codpro = strip_tags(isset($request["codpro"]) ? $request["codpro"] : "no especificado");
            $idpro = strip_tags(isset($request["idpro"]) ? $request["idpro"] : "no especificado");
            $asunto = "Contacto desde Inmueble $codpro en Pagina web";
            $subjects = strip_tags(isset($request["subject"]) ? $request["subject"] : "no especificado");
            $branch = strip_tags(isset($request["branch"]) ? $request["branch"] : "no especificado");

            $message = file_get_contents("views/mail-templates/detalle_send_codpro.html");
            $message = str_replace("%asunto%", $asunto, $message);
            $message = str_replace("%fecha%", $fecha, $message);
            $message = str_replace("%name%", $name, $message);
            $message = str_replace("%email%", $email, $message);
            $message = str_replace("%phone%", $phone, $message);
            $message = str_replace("%codpro%", $codpro, $message);
            $message = str_replace("%branch%", $branch, $message);
            $message = str_replace("%subjects%", $subjects, $message);
            $message = str_replace("%message%", $messages, $message);
        } else if ($tipo == 3) {
            //Formulario desde detalle del inmueble para un amigo
            $sendmail = strip_tags(isset($request["sendmail"]) ? $request["sendmail"] : "no especificado");
            $codpro = strip_tags(isset($request["codpro"]) ? $request["codpro"] : "no especificado");
            $url = strip_tags(isset($request["url"]) ? $request["url"] : "no especificado");
            $title = strip_tags(isset($request["title"]) ? $request["title"] : "no especificado");
            $description = strip_tags(isset($request["description"]) ? $request["description"] : "no especificado");
            $asunto = "Te recomiendo este Inmueble $codpro en Uniproyectos";

            $message = file_get_contents("views/mail-templates/detalle_send_friend.html");
            $message = str_replace("%asunto%", $asunto, $message);
            $message = str_replace("%fecha%", $fecha, $message);
            $message = str_replace("%name%", $name, $message);
            $message = str_replace("%email%", $email, $message);
            $message = str_replace("%sendmail%", $sendmail, $message);
            $message = str_replace("%phone%", $phone, $message);
            $message = str_replace("%codpro%", $codpro, $message);
            $message = str_replace("%url%", $url, $message);
            $message = str_replace("%title%", $title, $message);
            $message = str_replace("%description%", $description, $message);
            $message = str_replace("%message%", $messages, $message);

            $destinatario2 = $email;
            $destinatario3 = $sendmail;
        } else if ($tipo == 4) {
            //Formulario de contacto genérico del index
            $asunto = "Contacto desde Pagina web";
            $subjects = "Mensaje enviado desde contaco de Pagina Web";
            $sendmail = strip_tags(isset($request["sendmail"]) ? $request["sendmail"] : "no especificado");

            $message = file_get_contents("views/mail-templates/detalle_send.html");
            $message = str_replace("%asunto%", $asunto, $message);
            $message = str_replace("%fecha%", $fecha, $message);
            $message = str_replace("%name%", $name, $message);
            $message = str_replace("%email%", $email, $message);
            $message = str_replace("%phone%", $phone, $message);
            $message = str_replace("%subjects%", $subjects, $message);
            $message = str_replace("%message%", $messages, $message);

            $destinatario4 = $sendmail;
        }

        //print_r($request); die;

        // echo $message;die;

        //Create a new PHPMailer instance
        $mail = new PHPMailer();
        $mail->isSMTP();
        $mail->SMTPDebug = 0;
        $mail->Host = $mailsettings["host"];
        $mail->Port = $mailsettings["port"];
        $mail->SMTPSecure = "tls";
        $mail->SMTPAuth = true;
        $mail->Username = $mailsettings["username"];
        $mail->Password = $mailsettings["password"];
        $mail->setFrom($mailsettings["fromusername"], "Pagina Web Uniproyectos");
        $mail->addAddress($destinatario, "Pagina Web Uniproyectos");

        if ($destinatario2 != "") {
            $mail->addAddress($destinatario2, $nameFriend);
        }

        if ($destinatario3 != "") {
            $mail->addAddress($destinatario3, "Recomendación desde Uniproyectos");
        }

        if ($destinatario4 != "") {
            $mail->addAddress($destinatario4, "Pagina Web Uniproyectos");
        }

        //$mail->addAddress("desarrolloweb@domus.la", "Pagina Web Internegocios");
        $mail->Subject = $asunto;
        $mail->msgHTML($message);
        $mail->CharSet = "UTF-8";

        //send the message, check for errors
        if (!$mail->send()) {
            $data = array(
                "status" => 0,
                "message" => "Problema al enviar el correo",
                "usuario" => $mailsettings["fromusername"],
                "contraseña" => $mailsettings["password"],
            );

            return Flight::json($data);
        } else {
            $data = array(
                "status" => 1,
                "message" => "se envio con exito el correo",
            );

            return Flight::json($data);
        }
    }
}

<?php

namespace app;

// Controladores
use App\FunctionsController;

// Paquetes
use Jenssegers\Blade\Blade;
use GuzzleHttp\Client;

trait AuthorizationController {

    protected Blade $blade;
    protected String $token;
    protected FunctionsController $functions;
    protected String $grupo;
    protected String $endpoint;
    protected String $inmobiliaria;
    protected Client $client;

    public function __construct() {
        if (!isset($_SESSION)) {
            session_start();
        }

        $this->blade = new Blade("views", "cache");

        $dotenv = \Dotenv\Dotenv::createMutable($_SERVER["DOCUMENT_ROOT"]);
        $dotenv->load();

        $this->token = $_ENV["TOKEN"];
        $this->functions = new FunctionsController();
        $this->grupo = $_ENV["GRUPO"];
        $this->inmobiliaria = $_ENV["INMOBILIARIA"];
        $this->endpoint = "https://api.domus.la/3.0/";

        $this->client = new Client();
    }

    // Función que muestra los tipos de Inmueble
    public function tipos() {
        if (!isset($_SESSION["webpage_types"]) || $_SESSION["webpage_types"] == null) {

            $res = $this->client->request("GET", $this->endpoint . "search/types", [
                "headers" => [
                    "Authorization" => $this->token,
                    "inmobiliaria" => $this->inmobiliaria,
                    "grupo" => $this->grupo,
                ],
            ]);

            $tipos = json_decode($res->getBody(), true);

            $_SESSION["webpage_types"] = $tipos;
        }

        return $_SESSION["webpage_types"];
    }

    // Función para ver todos los grupos de la inmobiliaria
    public function grupos() {
        if (!isset($_SESSION["webpage_groups"]) || $_SESSION["webpage_groups"] == null) {

            $res = $this->client->request("GET", $this->endpoint . "administrative/groups", [
                "headers" => [
                    "Authorization" => $this->token,
                ],
            ]);

            $grupos = json_decode($res->getBody(), true);

            $_SESSION["webpage_groups"] = $grupos;
        }

        return $_SESSION["webpage_groups"];
    }

    // Función que muestra las ciudades de Inmueble
    public function ciudades() {

        if (!isset($_SESSION["webpage_cities"]) || $_SESSION["webpage_cities"] == null) {

            $res = $this->client->request("GET", $this->endpoint . "search/cities", [
                "headers" => [
                    "Authorization" => $this->token,
                    "inmobiliaria" => $this->inmobiliaria,
                    "grupo" => $this->grupo,
                ],
            ]);

            $ciudades = json_decode($res->getBody(), true);

            $_SESSION["webpage_cities"] = $ciudades;
        }

        return $_SESSION["webpage_cities"];
    }

    // Función que muestra las zonas de Inmueble
    public function zonas() {

        if (!isset($_SESSION["webpage_zones"]) || $_SESSION["webpage_zones"] == null) {

            $res = $this->client->request("GET", $this->endpoint . "search/zones", [
                "headers" => [
                    "Authorization" => $this->token,
                    "inmobiliaria" => $this->inmobiliaria,
                    "grupo" => $this->grupo,
                ],
            ]);

            $zonas = json_decode($res->getBody(), true);

            $_SESSION["webpage_zones"] = $zonas;
        }

        return $_SESSION["webpage_zones"];
    }

    // Función que muestra las gestiones de Inmueble
    public function gestiones() {

        if (!isset($_SESSION["webpage_biz"]) || $_SESSION["webpage_biz"] == null) {

            $res = $this->client->request("GET", $this->endpoint . "search/biz", [
                "headers" => [
                    "Authorization" => $this->token,
                    "inmobiliaria" => $this->inmobiliaria,
                    "grupo" => $this->grupo,
                ],
            ]);

            $biz = json_decode($res->getBody(), true);

            $_SESSION["webpage_biz"] = $biz;
        }

        return $_SESSION["webpage_biz"];
    }

    // Función que muestra los inmuebles recientes
    public function inmuebles($cantidad = 1, $filtro = "", $json = 0) {

        $res = $this->client->request("GET", $this->endpoint . "properties?status=1&$filtro", [
            "headers" => [
                "Authorization" => $this->token,
                "perpage" => $cantidad,
                "inmobiliaria" => $this->inmobiliaria,
                "grupo" => $this->grupo,
            ],
        ]);

        if ($json == 0) {
            return json_decode($res->getBody(), true);
        } else {
            return $res->getBody();
        }
    }

    // Método para obtener inmuebles para el mapa
    public function inmueblesMapa($cantidad = 12, $filtro = 11, $json = 0) {

        $res = $this->client->request("GET", $this->endpoint . "properties/map?status=1&" . $filtro, [
            "headers" => [
                "Authorization" => $this->token,
                "inmobiliaria" => $this->inmobiliaria,
                "grupo" => $this->grupo,
                "perpage" => $cantidad,
            ],
        ]);

        if ($json == 1) {
            return json_decode($res->getBody(), true);
        } else {
            return $res->getBody();
        }
    }

    // Detalle del inmueble
    public function detalle($codigo, $id, $json = 0) {

        $res = $this->client->request("GET", $this->endpoint . "properties/$codigo/$id", [
            "headers" => [
                "Authorization" => $this->token,
                "inmobiliaria" => $this->inmobiliaria,
                "grupo" => $this->grupo,
            ],
        ]);

        if ($json == 0) {
            return json_decode($res->getBody(), true);
        } else {
            return $res->getBody();
        }
    }

    // Detalle del inmueble
    public function similares($inmueble, $cantidad, $json = 0) {

        if ($inmueble["biz_code"] == 1) {
            $pricemin_name = "pcmin";
            $pricemax_name = "pcmax";
            $pricemin = ($inmueble["rent"] - 500000);
            $pricemax = ($inmueble["rent"] + 500000);
        } else {
            $pricemin_name = "pvmin";
            $pricemax_name = "pvmax";
            $pricemin = ($inmueble["saleprice"] - 10000000);
            $pricemax = ($inmueble["saleprice"] + 10000000);
        }

        $res = $this->client->request("GET", $this->endpoint . "properties?biz={$inmueble["biz_code"]}&type={$inmueble["type_code"]}&" . $pricemin_name . "=" . $pricemin . "&" . $pricemax_name . "=" . $pricemax, [
            "headers" => [
                "Authorization" => $this->token,
                "perpage" => $cantidad,
                "inmobiliaria" => $this->inmobiliaria,
                "grupo" => $this->grupo,
            ],
        ]);

        if ($json == 0) {
            return json_decode($res->getBody(), true);
        } else {
            return $res->getBody();
        }
    }
}

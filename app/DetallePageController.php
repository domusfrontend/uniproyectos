<?php

namespace app;

class DetallePageController {

    use AuthorizationController;

    public function index($id, $codigo) {
        $inmueble = $this->detalle($codigo, $id)["data"];

        if ($inmueble["images"]) {
            $meta_image = $inmueble["images"][0]["imageurl"];
        } else {
            $meta_image = "/assets/img/logo.png";
        }

        $meta_description = substr($inmueble["description"], 0, 100) . "...";
        $meta_title = ucfirst(strtolower($inmueble["type"])) . " en " . strtolower($inmueble["biz"]) . " en " . strtolower($inmueble["neighborhood"]);
        $meta_url = "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";

        $imagenes = [];
        if ($inmueble["images"]) {
            $data = getimagesize($inmueble["images"][0]["imageurl"]);
            // $data = [500, 500];
            $width = $data[0];
            $height = $data[1];

            foreach ($inmueble["images"] as $picture) {
                array_push($imagenes, $picture["imageurl"]);
            }

        } else {
            $width = "500";
            $height = "500";
        }

        if ($inmueble["status"] != 1) {
            $index = new IndexPageController();
            return $index->error404();
        }

        // Armamos la url para las fotos 360
        $urlFotos = "https://fotos360.domus.la/fotos-360-domus/";
        $urlFotos .= base64_encode($this->token) . "/";
        $urlFotos .= base64_encode($inmueble["codpro"]) . "/";
        $urlFotos .= base64_encode($this->grupo);

        echo $this->blade->make("templates.header", [
            "clase" => $this->functions,
            "titulo" => $meta_title,
            "detalle" => 2,
            "width" => $width,
            "height" => $height,
            "meta_image" => $meta_image,
            "descripcion" => $meta_description,
            "meta_url" => $meta_url,
            "activeInmuebles" => "active",
            "codigo" => $codigo,
        ])->render();

        echo $this->blade->make("detalle", [
            "codigo" => $codigo,
            "clase" => $this->functions,
            "token" => $this->token,
            "imagenes" => $imagenes,
            "inmueble" => $inmueble,
            "meta_url" => $meta_url,
            "titulo" => $meta_title,
            "descripcion" => $meta_description,
            "ciudades" => $this->ciudades(),
            "tipos" => $this->tipos(),
            "domulares" => $this->similares($inmueble, 5),
            "urlFotos" => $urlFotos,
        ])->render();

        echo $this->blade->make("templates.footer")->render();
    }

}

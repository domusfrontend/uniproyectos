<?php

namespace app;

class IndexPageController {

    use AuthorizationController;

    public function index() {
        // Inmuebles destacados
        if (!isset($_SESSION["webpage_destacados"]) || $_SESSION["webpage_destacados"] == null) {
            $destacados = $this->inmuebles(7, "great=on");
        } else {
            $destacados = $_SESSION["webpage_destacados"];
        }

        echo $this->blade->make("templates.header", [
            "clase" => $this->functions,
            "titulo" => "Inicio",
            "activeInicio" => "active",
        ])->render();

        echo $this->blade->make("index", [
            "clase" => $this->functions,
            "tipos" => $this->tipos(),
            "ciudades" => $this->ciudades(),
            "zonas" => $this->zonas(),
            "destacados" => $destacados["data"],
        ])->render();

        echo $this->blade->make("templates.footer")->render();
    }

    public function mapa() {
        echo $this->blade->make("templates.header", [
            "clase" => $this->functions,
            "titulo" => "Inicio",
            "activeInicio" => "active",
            "styles" => [
                "mapa"
            ]
        ])->render();

        echo $this->blade->make("mapa", [
            "clase" => $this->functions,
        ])->render();

        echo $this->blade->make("templates.footer")->render();
    }

    public function error404() {
        header("HTTP/1.0 404 Not Found");

        echo $this->blade->make("templates.header", [
            "clase" => $this->functions,
            "titulo" => "Error 404",
            "descripcion" => "Página no encontrada",
            "canonical" => $this->functions->getRequestURI(false),
        ])->render();

        echo $this->blade->make("404")->render();

        echo $this->blade->make("templates.footer")->render();
    }

    public function form() {
        return $this->functions->generic_form($_REQUEST, $_FILES);
    }

    public function clearCache() {
        echo $this->functions->clearCache();
    }
}

<?php

namespace app;

class ListaPageController {

    use AuthorizationController;

    public function index() {

        /*
        En este punto se recoge la url y se tranforma para poder ser usada
        a la hora de obtener los filtros respectivos
         */
        $actual_link = $_SERVER["REQUEST_URI"];

        $limpia1 = str_replace("/busqueda/", "", $actual_link);
        $arrayvariables = explode("/", $limpia1);
        $arrayurl = [];
        $tamano = sizeof($arrayvariables);

        /*
        En este punto se obtienen los diferentes parámetros de búsqueda
        Y dependiendo de si son o no pares, se asignan a un key y un value en
        el array
         */
        for ($x = 0; $x <= $tamano; $x++) {
            if ($x % 2 == 0) {
                if ($x < $tamano) {
                    $next = $x + 1;
                    $arrayurl[$x] = [$arrayvariables[$x] => $arrayvariables[$next]];
                }
            }
        }

        $aplanado = $this->functions->flatten($arrayurl, "");

        // Ciudades
        $ciudadtransformer = array_map(function ($operator) {
            $ciudad = $this->functions->replacespacios($operator["name"], " ");
            return [
                str_replace(" ", "-", $ciudad) => $operator["code"],
            ];
        }, $this->ciudades()["data"]);
        $ciudadplana = $this->functions->flatten($ciudadtransformer, "");

        // Zonas
        $zonestransformer = array_map(function ($operator) {
            $zonas = $this->functions->replacespacios($operator["name"], " ");
            return [
                $zonas => $operator["code"],
            ];
        }, $this->zonas()["data"]);
        $zonesplana = $this->functions->flatten($zonestransformer, "");

        // Tipos de inmueble
        $typestransformer = array_map(function ($operator) {
            $tipos = $this->functions->replacespacios($operator["name"], " ");
            return [
                str_replace(" ", "-", $tipos) => $operator["code"],
            ];
        }, $this->tipos()["data"]);
        $typesplana = $this->functions->flatten($typestransformer, "");

        // Gestiones
        $biztransformer = array_map(function ($operator) {
            $gestiones = $this->functions->replacespacios($operator["name"], " ");
            return [
                str_replace("/", "-", $gestiones) => $operator["code"],
            ];
        }, $this->gestiones()["data"]);
        $bizplana = $this->functions->flatten($biztransformer, "");

        // Comienzo de armado del filtro
        $filtro = "";
        $codigo = "";
        $ciudad = "";
        $tipo = "";
        $tipoName = "inmuebles";
        $gestion = "";
        $gestionName = "arriendo y venta";
        $zona = "";
        $preciomin = "";
        $preciomax = "";
        $arrmin = "";
        $arrmax = "";
        $areamin = "";
        $areamax = "";
        $barrio = "";
        $orden = "";
        $minhabitaciones = "";
        $maxhabitaciones = "";
        $habmax = "";
        $minbanos = "";
        $maxbanos = "";
        $minparkea = "";
        $maxparkea = "";
        $page = "";
        $minage = "";
        $maxage = "";
        $amenities = "";
        $destacado = "";
        $comparativo = "";
        $sede = "";
        $estrato = "";
        $frame = 0;

        if (array_key_exists("codigo", $aplanado)) {
            $filtro .= "codpro=" . $aplanado["codigo"] . "&";
            $codigo .= $aplanado["codigo"];
        }

        if (array_key_exists("iframe", $aplanado) && $aplanado["iframe"] == 1) {
            $frame = 1;
        }

        if (array_key_exists("pagina", $aplanado)) {
            $filtro .= "page=" . $aplanado["pagina"] . "&";
            $page = $aplanado["pagina"];
        }

        if (array_key_exists("destacado", $aplanado)) {
            $filtro .= "great=" . $aplanado["destacado"] . "&";
            $destacado = $aplanado["destacado"];
        }

        if (array_key_exists("barrio", $aplanado)) {
            $filtro .= "neighborhood=" . str_replace("-", " ", $aplanado["barrio"]) . "&";
            $barrio = str_replace("-", " ", urldecode($aplanado["barrio"]));
        }

        if (array_key_exists("sede", $aplanado)) {
            $filtro .= "branch=" . $aplanado["sede"] . "&";
            $sede = $aplanado["sede"];
        }

        if (array_key_exists("minhabitaciones", $aplanado)) {
            $filtro .= "minbed=" . $aplanado["minhabitaciones"] . "&";
            $minhabitaciones = $aplanado["minhabitaciones"];
        }

        if (array_key_exists("maxhabitaciones", $aplanado)) {
            $filtro .= "maxbed=" . $aplanado["maxhabitaciones"] . "&";
            $maxhabitaciones = $aplanado["maxhabitaciones"];
        }

        if (array_key_exists("minbanos", $aplanado)) {
            $filtro .= "minbath=" . $aplanado["minbanos"] . "&";
            $minbanos = $aplanado["minbanos"];
        }

        if (array_key_exists("maxbanos", $aplanado)) {
            $filtro .= "maxbath=" . $aplanado["maxbanos"] . "&";
            $maxbanos = $aplanado["maxbanos"];
        }

        if (array_key_exists("minparkea", $aplanado)) {
            $filtro .= "minparking=" . $aplanado["minparkea"] . "&";
            $minparkea = $aplanado["minparkea"];
        }

        if (array_key_exists("maxparkea", $aplanado)) {
            $filtro .= "maxparking=" . $aplanado["maxparkea"] . "&";
            $maxparkea = $aplanado["maxparkea"];
        }

        if (array_key_exists("maxbanos", $aplanado)) {
            $filtro .= "maxbath=" . $aplanado["maxbanos"] . "&";
            $maxbanos = $aplanado["maxbanos"];
        }

        if (array_key_exists("areamin", $aplanado)) {
            $filtro .= "minarea=" . $aplanado["areamin"] . "&";
            $areamin = $aplanado["areamin"];
        }

        if (array_key_exists("areamax", $aplanado)) {
            if (!isset($aplanado["areamin"]) || $aplanado["areamin"] < 1000) {
                $filtro .= "maxarea=" . $aplanado["areamax"] . "&";
            }
            
            $areamax = $aplanado["areamax"];
        }

        if (array_key_exists("estrato", $aplanado)) {
            $filtro .= "stratum=" . $aplanado["estrato"] . "&";
            $estrato = $aplanado["estrato"];
        }

        if (array_key_exists("amenities", $aplanado)) {
            $filtro .= "amenities=" . $aplanado["amenities"] . "&";
            $amenities = $aplanado["amenities"];
        }

        if (array_key_exists("ciudad", $aplanado)) {
            $x = $ciudadplana[str_replace("ñ", "Ñ", urldecode($aplanado["ciudad"]))];
            $filtro .= "city=" . $x . "&";
            $ciudad = $x;
            foreach ($this->ciudades()["data"] as $ciudades) {
                if ($ciudades["code"] == $x) {
                    $ciudad = $ciudades["code"];
                }
            }
        }

        if (array_key_exists("tipo", $aplanado)) {
            $x = $typesplana[$aplanado["tipo"]];
            $filtro .= "type=" . $x . "&";
            $tipo = $x;
            foreach ($this->tipos()["data"] as $type) {
                if ($type["code"] == $x) {
                    $tipo = $type["code"];
                    $tipoName = $type["name"];
                }
            }
        }

        if (array_key_exists("gestion", $aplanado)) {
            $x = $bizplana[$aplanado["gestion"]];
            $filtro .= "biz=" . $x . "&";
            $gestion = $x;
            foreach ($this->gestiones()["data"] as $biz) {
                if ($biz["code"] == $x) {
                    $gestion = $biz["code"];
                    $gestionName = $this->functions->toLowerCase($biz["name"]);
                }
            }
        }

        if (array_key_exists("preciomin", $aplanado)) {
            if ($gestion == 1) {
                $filtro .= "pcmin=" . $aplanado["preciomin"] . "&";    
            } else {
                $filtro .= "pvmin=" . $aplanado["preciomin"] . "&";
            }

            $preciomin = $aplanado["preciomin"];
        }

        if (array_key_exists("preciomax", $aplanado)) {
            if ($gestion == 1) {
                $filtro .= "pcmax=" . $aplanado["preciomax"] . "&";
            } else {
                $filtro .= "pvmax=" . $aplanado["preciomax"] . "&";
            }

            $preciomax = $aplanado["preciomax"];
        }

        if (array_key_exists("zona", $aplanado)) {
            $x = $zonesplana[$aplanado["zona"]];
            $filtro .= "zone=" . $x . "&";
            $zona = $x;
            foreach ($this->zonas()["data"] as $zone) {
                if ($zone["code"] == $x) {
                    $zona = $zone["code"];
                }
            }
        }

        if (array_key_exists("comparativo", $aplanado)) {
            $comparativo = $aplanado["comparativo"];
        }

        if ($gestion == 1) {
            $active = "activeArriendo";
        } else {
            $active = "activeVenta";
        }

        $inmuebles = $this->inmuebles(12, $filtro);

        // Armamos el título de lista
        $tipoName = $this->functions->pluralizeWord($tipoName, $inmuebles["total"], true);
        $titulo = "{$inmuebles["total"]} {$tipoName} en {$gestionName}";

        echo $this->blade->make("templates.header", array(
            "clase" => $this->functions,
            "titulo" => "Lista de inmuebles",
            "activeInmuebles" => "active",
            "activeArriendo" => $gestion != 2 ? "active" : "",
            "activeVenta" => $gestion != 1 ? "active" : "",
            "descripcion" => "En Cityraiz Inmobiliaria Vendemos, Arrendamos o Anunciamos tu Inmueble. Brindamos Tranquilidad a Nuestros Clientes y Servicio Profesional.",
            "canonical" => $this->functions->getRequestURI(),
        ))->render();

        echo $this->blade->make("lista", array(
            "clase" => $this->functions,
            "gestion" => $gestion,
            "ciudads" => $ciudad,
            "zonass" => $zona,
            "tiposs" => $tipo,
            "minhabitaciones" => $minhabitaciones,
            "maxhabitaciones" => $maxhabitaciones,
            "minbanos" => $minbanos,
            "maxbanos" => $maxbanos,
            "minparkea" => $minparkea,
            "maxparkea" => $maxparkea,
            "comparativo" => $comparativo,
            "preciomin" => $preciomin,
            "preciomax" => $preciomax,
            "areamin" => $areamin,
            "areamax" => $areamax,
            "codigo" => $codigo,
            "estrato" => $estrato,
            "amenities" => $amenities,
            "ciudades" => $this->ciudades(),
            "tipos" => $this->tipos(),
            "zonas" => $this->zonas(),
            "inmuebles" => $inmuebles,
            "clase" => $this->functions,
            "titulo" => $titulo,
            "barrio" => $barrio,
            "filtro" => $filtro,
            "filterForMap" => isset($aplanado["pagina"]) && $aplanado["pagina"] != "" ? str_replace("page=" . $aplanado["pagina"] . "&", "pagina=1&", $filtro) : $filtro,
        ))->render();

        echo $this->blade->make("templates.footer")
            ->render();
    }

}

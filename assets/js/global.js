Vue.createApp({
    data() {
        return {
            showInmuebles: "1",
            detailModalOptions: null,
            gestion: "",
            precioDesde: 0,
            precioHasta: 0,
            areaDesde: 1,
            areaHasta: 1,
            amenities: [],
            email: "",

            // Variables de mapa
            map: null,
            inmuebles: [],
            marcadores: [],
            drawingManager: null,
            infowindow: null
        }
    },
    mounted() {
        this.detail();
        this.mapaInmuebles();

        $(".card").click(function () {
            $(this).toggleClass("flipped");
        });

        if ($("#myModal").length) {
            // $("#myModal").modal("show");
        }
    },
    methods: {
        // Método para armar la url de los filtros de inmuebles
        search(pagina = "1", url = "/busqueda") {
            url += "/pagina/" + pagina;

            var ids = [
                "tipofiltro",
                "ciudadfiltro",
                "preciominfiltro",
                "preciomaxfiltro",
                "areaminfiltro",
                "areamaxfiltro",
                "gestionfiltro",
                "barriofiltro",
                "zonafiltro",
                "codigofiltro",
                "estratofiltro",
                "minhabitacionesfiltro",
                "maxhabitacionesfiltro",
                "minbanosfiltro",
                "maxbanosfiltro",
                "minparkeafiltro",
                "maxparkeafiltro",
            ];

            var filtros = {};

            ids.forEach((item, i) => {
                if ($("#" + item).length > 0) {
                    if (typeof $("#" + item).val() !== "undefined") {
                        filtros[item.replace("filtro", "")] = $("#" + item).val();
                    } else if (typeof $("#" + item + " option:selected").val() !== "undefined") {
                        filtros[item.replace("filtro", "")] = $("#" + item + "option:selected").val();
                    }
                }
            });

            Object.entries(filtros).forEach(([key, value]) => {
                if (value != "" && typeof value !== "undefined" && value != null) {
                    var valorLimpio = value.toString().toLowerCase().trim().replace(/\s/g, "-");

                    if (key == "codigo") {
                        url = "/busqueda/pagina/1/codigo/" + valorLimpio;
                    } else {
                        valorLimpio = valorLimpio.replace("/", "-");
                        valorLimpio = valorLimpio.replace(".", "");
                        valorLimpio = valorLimpio.replace(",", "");
                        url += "/" + key + "/" + valorLimpio;
                    }
                }
            });

            // console.log(url); return false;

            window.location.href = url;
        },

        setEmail(email) {
            this.email = email;
        },

        formatPrice(value) {
            if (value != "") {
                return "$ " + value.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ',');
            } else {
                return value;
            }
        },

        // Filtro para el video
        videoURL(url) {
            const regExp = /^.*(youtu.be\/|v\/|u\/\w\/|embed\/|watch\?v=|&v=)([^#&?]*).*/;
            const match = url.match(regExp);
            var videoId = (match && match[2].length === 11) ? match[2] : null;

            return "https://www.youtube.com/embed/" + videoId;
        },

        detail() {
            if ($("#imageGallery").length) {
                $("#imageGallery").lightSlider({
                    gallery: true,
                    item: 1,
                    loop: true,
                    thumbItem: 9,
                    slideMargin: 0,
                    enableDrag: false,
                    currentPagerPosition: "left"
                });
            }
        },

        /**
         * mapaInmuebles
         * Mapa de inmuebles con buscador con dibujo en polígono
         */
        mapaInmuebles() {
            if ($("#map").length) {

                // Quitamos el logo de cargando
                $("#carga").hide();

                // Posición inicial del mapa
                const myLatLng = { lat: 4.60971, lng: -74.08175 };

                document
                    .getElementById("draw-map")
                    .addEventListener("click", this.addDrawControl);
                document
                    .getElementById("clear-map")
                    .addEventListener("click", this.cleanMarkers);

                // Declaramos el mapa
                this.map = new google.maps.Map(document.getElementById("map"), {
                    zoom: 15,
                    center: myLatLng,
                });
            }
        },

        cleanMarkers() {
            for (let i = 0; i < this.marcadores.length; i++) {
                this.marcadores[i].setMap(null);
                this.marcadores[i].setVisible(false);
            }

            this.marcadores = [];
        },

        addDrawControl() {
            // Añadimos el control para permitir el dibujo de polígono sobre el mapa
            if (this.drawingManager == null) {
                this.drawingManager = new google.maps.drawing.DrawingManager({
                    drawingMode: google.maps.drawing.OverlayType.POLYLINE,
                    drawingControl: true,
                    drawingControlOptions: {
                        position: google.maps.ControlPosition.TOP_CENTER,
                        drawingModes: [
                            google.maps.drawing.OverlayType.POLYLINE,
                        ],
                    }
                });
            }
            this.drawingManager.setMap(this.map);
            var self = this;

            this.cleanMarkers();

            // Controlamos lo que ocurre luego que se ha terminado de dibujar sobre el mapa
            google.maps.event.addListener(this.drawingManager, "overlaycomplete", function (event) {
                // Obtenemos las coordenadas
                var locations = event.overlay.getPath().getArray().map(coord => {
                    return {
                        lat: coord.lat(),
                        lng: coord.lng()
                    }
                });

                // Ahora sí hacemos la búsqueda en el API
                self.drawingManager.setMap(null);
                // Quitamos el dibujo que se hizo en el mapa
                event.overlay.setMap(null);

                // Añadimos el logo de cargando
                $("#carga").show();

                self.infowindow = new google.maps.InfoWindow({
                    content: "",
                    maxWidth: 500,
                });

                fetch("/filtro/mapa/1000/polygon=" + JSON.stringify([[locations]])).then(response => response.json())
                    .then(data => {
                        // Quitamos el logo de cargando
                        $("#carga").hide();
                        var map = self.map;

                        // Agregamos los marcadores al mapa
                        data.data.forEach(item => {
                            var marker = new google.maps.Marker({
                                position: { lat: parseFloat(item.latitude), lng: parseFloat(item.longitude) },
                                map
                            });

                            marker.addListener("click", () => {
                                self.infowindow.setContent("Espere un momento...");
                                self.infowindow.open(map, marker);
                                self.openDetail(item.codpro, item.idpro);
                            });

                            self.marcadores.push(marker);
                        });
                    });
            });
        },

        openDetail(codpro, idpro) {
            var self = this;
            fetch("/filtro-detalle/mapa/" + codpro + "/" + idpro).then(response => response.json())
                .then(data => {
                    const respuesta = data.data;
                    const redir = "/inmuebles/" + respuesta.type.toLowerCase() + "-en" + respuesta.biz.toLowerCase() + "-en-" + respuesta.neighborhood.toLowerCase() + "/" + respuesta.idpro + "/" + respuesta.codpro;
                    const imagen = respuesta.images.length > 0 ? respuesta.images[0].imageurl : "/assets/img/logo/logo.png";
                    const urlinspacios = redir.replace(/\s/g, "-");

                    const contentString = ''
                        + '<div class="map-info-window">'
                        + '<div class="item-wrap">'
                        + '<div class="item-header">'
                        + '<a class="hover-effect" href="' + urlinspacios + '" tabindex="0" target="_blank">'
                        + '<img style="width: 100%; height: 100px; object-fit:cover;" class="listing-thumbnail img-domus-map" src="' + imagen + '">'
                        + '</a>'
                        + '</div>'
                        + '<div class="item-body flex-grow-1">'
                        + '<ul class="list-unstyled item-info">'
                        + '<li class="item-price">'
                        + '<span class="price-prefix">COP</span> ' + respuesta.price_format
                        + '</li>'
                        + '</ul>'
                        + '</div>'
                        + '</div>'
                        + '</div>'
                        + '</div>'
                        + '<button draggable="false" aria-label="Close" title="Close" type="button" class="gm-ui-hover-effect" style="background: none; display: block; border: 0px; margin: 0px; padding: 0px; text-transform: none; appearance: none; position: absolute; cursor: pointer; user-select: none; top: -6px; right: -6px; width: 30px; height: 30px;">'
                        + '<img src="data:image/svg+xml,%3Csvg%20xmlns%3D%22http%3A//www.w3.org/2000/svg%22%20viewBox%3D%220%200%2024%2024%22%3E%3Cpath%20d%3D%22M19%206.41L17.59%205%2012%2010.59%206.41%205%205%206.41%2010.59%2012%205%2017.59%206.41%2019%2012%2013.41%2017.59%2019%2019%2017.59%2013.41%2012z%22/%3E%3Cpath%20d%3D%22M0%200h24v24H0z%22%20fill%3D%22none%22/%3E%3C/svg%3E" alt="" style="pointer-events: none; display: block; width: 14px; height: 14px; margin: 8px;">'
                        + '</button>'
                        + '</div>';

                    self.infowindow.setContent(contentString);
                });
        }
    }
}).mount("#content-page");

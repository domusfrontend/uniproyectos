<div class="main-wrapper" id="content-page">
    <div class="main">
        <div class="main-inner">
            <div class="content">
                <div class="container">
                    <div class="row">


                        <div class="listing-detail col-md-8 col-lg-9">
                            <div class="property-single">
                                <ul id="imageGallery">
                                    @foreach ($imagenes as $imagen)
                                        <li data-thumb="{{ $imagen }}" style="max-width: 100%;">
                                            <img src="{{ $imagen }}" style="max-width: 100%; " />
                                        </li>
                                    @endforeach
                                </ul>

                            </div>

                            <div class="row">
                                <div class="col-lg-5">
                                    <div class="overview">
                                        <h2>Información</h2>

                                        <ul>
                                            <li>
                                                <strong>Código</strong><span>{{ $inmueble["codpro"] }}</span>
                                            </li>
                                            <li>
                                                <strong>Barrio</strong><span>{{ $inmueble["neighborhood"] }}</span>
                                            </li>
                                            <li>
                                                <strong>Precio</strong><span>{{ $inmueble["price_format"] }}</span>
                                            </li>
                                            <li>
                                                <strong>Administración</strong><span>{{ $clase->priceFormat($inmueble["administration"]) }}</span>
                                            </li>
                                            <li>
                                                <strong>Ciudad</strong><span>{{ $clase->cletter($inmueble["city"]) }}</span>
                                            </li>
                                            <li>
                                                <strong>Gestión</strong><span>{{ $clase->cletter($inmueble["biz"]) }}</span>
                                            </li>
                                            <li>
                                                <strong>Habitaciones</strong><span>{{ $inmueble["bedrooms"] }}</span>
                                            </li>
                                            <li>
                                                <strong>Baños</strong><span>{{ $inmueble["bathrooms"] }}</span>
                                            </li>
                                            <li>
                                                <strong>Parqueaderos</strong><span>{{ $inmueble["parking"] }}</span>
                                            </li>
                                            <li>
                                                <strong>Área</strong><span>{{ $inmueble["area_cons"] }} M2</span>
                                            </li>
                                            <li>
                                                <strong>Estrato</strong><span>{{ $inmueble["stratum"] }}</span>
                                            </li>
                                        </ul>
                                    </div><!-- /.overview -->
                                </div><!-- /.col-* -->

                                <div class="col-lg-7">
                                    <h2>Descripción</h2>

                                    <p>
                                        {{ $inmueble["description"] }}
                                    </p>

                                </div><!-- /.col-* -->
                            </div><!-- /.row -->



                            <h2>Caracteristicas</h2>

                            <ul class="amenities">
                                @foreach ($inmueble["amenities"] as $caract)
                                    <li class="yes">
                                        {{ $caract["name"] }}
                                    </li>
                                @endforeach
                            </ul>

                            <!-- Inmuebles Similares -->

                            <h2>Inmuebles Similares</h2>

                            <div class="row">

                                @foreach ($domulares["data"] as $domular)
                                    @if ($domular["idpro"] != $inmueble["idpro"])
                                        <div class="listing-row">
                                            <div class="listing-row-inner">
                                                <div class="listing-row-image" style="background-image: url('{{ $domular["image1"] }}');">
                                                    <span class="listing-row-image-links">
                                                        <a href="/inmuebles/{{ $clase->replacespacios($domular["type"]) }}-en-{{ $clase->replacespacios($domular["biz"]) }}-{{ $clase->replacespacios($domular["city"]) }}/{{ $domular["idpro"] }}/{{ $domular["codpro"] }}">
                                                            <i class="fa fa-search"></i>
                                                            <span>Ver detalle</span>
                                                        </a>
                                                    </span>
                                                </div>

                                                <div class="listing-row-content">
                                                    <h3>
                                                        <a href="/inmuebles/{{ $clase->replacespacios($domular["type"]) }}-en-{{ $clase->replacespacios($domular["biz"]) }}-{{ $clase->replacespacios($domular["city"]) }}/{{ $domular["idpro"] }}/{{ $domular["codpro"] }}">
                                                            {{ $clase->cletter($domular["type"]) }} en
                                                            {{ $clase->cletter($domular["neighborhood"]) }}
                                                        </a>
                                                    </h3>
                                                    <h4>{{ $domular["price_format"] }}</h4>

                                                    <ul class="listing-row-attributes">
                                                        <li>
                                                            <strong><i class="fa fa-map-marker"></i> Ciudad</strong>
                                                            <span>{{ $clase->cletter($domular["city"]) }}</span>
                                                        </li>

                                                        <li>
                                                            <strong><i class="fa fa-building"></i> Tipo</strong>
                                                            <span>{{ $clase->cletter($domular["type"]) }}</span>
                                                        </li>

                                                        <li>
                                                            <strong><i class="fa fa-certificate"></i> Gestión</strong>
                                                            <span>{{ $clase->cletter($domular["biz"]) }}</span>
                                                        </li>

                                                        <li>
                                                            <strong><i class="fa fa-arrows-alt"></i> Area</strong>
                                                            <span>{{ $domular["area_cons"] }} M2</span>
                                                        </li>

                                                        <li>
                                                            <strong><i class="fa fa-umbrella"></i> Baños</strong>
                                                            <span>{{ $domular["bathrooms"] }}</span>
                                                        </li>

                                                        <li>
                                                            <strong><i class="fa fa-bed"></i> Habitaciones</strong>
                                                            <span>{{ $domular["bedrooms"] }}</span>
                                                        </li>
                                                    </ul>
                                                </div><!-- /.listing-row-content -->
                                            </div><!-- /.listing-row-inner -->
                                        </div><!-- /.listing-row -->
                                    @endif
                                @endforeach

                            </div>

                            <!-- Final de Inmuebles similares -->

                        </div><!-- /.col-* -->

                        <div class="col-md-4 col-lg-3">
                            <div class="widget">
                                <ul class="nav nav-stacked nav-style-primary">

                                    <li class="nav-item">
                                        <a href="#" data-toggle="collapse" data-target="#demo" class="nav-link">
                                            <i class="fa fa-share-alt"></i> Compartir
                                        </a>

                                        <div id="demo" class="collapse collapse-share">

                                            <div class="row">
                                                <div class="col-md-3 social-icons whatsapp">
                                                    <a href="whatsapp://send?text={{ $meta_url }}" data-action="share/whatsapp/share">
                                                        <i class="fab fa-whatsapp"></i>
                                                    </a>
                                                </div>
                                                <div class="col-md-3 social-icons">
                                                    <a href="http://facebook.com/sharer.php?u={{ $meta_url }}" onclick="javascript:window.open(this.href, '', 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=400,width=700');return false;" target="_blank" title="Facebook">
                                                        <i class="fab fa-facebook-f"></i>
                                                    </a>
                                                </div>
                                                <div class="col-md-3 social-icons">
                                                    <a href="http://twitter.com/home?status={{ $clase->toLowerCase($inmueble["type"]) }}%20en%20{{ $clase->toLowerCase($inmueble["biz"]) }}%20en%20{{ $clase->toLowerCase($inmueble["city"]) }}%20{{ $meta_url }}" onclick="javascript:window.open(this.href, '', 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=400,width=700');return false;" target="_blank" title="Twitter">
                                                        <i class="fab fa-twitter"></i>
                                                    </a>
                                                </div>
                                                <div class="col-md-3 social-icons">
                                                    <a href="https://plus.google.com/share?url={{ $meta_url }}&amp;title={{ $clase->cletter($inmueble["type"]) }} {{ $inmueble["neighborhood"] }}" onclick="javascript:window.open(this.href, '', 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=400,width=700');return false;" target="_blank" title="Google+">
                                                        <i class="fab fa-google-plus"></i>
                                                    </a>
                                                </div>
                                            </div>

                                        </div>
                                    </li><!-- /.nav-item -->

                                    <li class="nav-item">
                                        <a href="#" data-toggle="modal" data-target="#modalSendEmail" class="nav-link">
                                            <i class="fa fa-envelope"></i> Enviar por correo
                                        </a>
                                    </li><!-- /.nav-item -->
                                </ul><!-- /.nav-style-primary -->
                            </div><!-- /.widget -->

                            <div class="widget widget-background-white">
                                <h3 class="widgettitle">Contactar</h3>

                                <form method="post" action="#" id="formmail">

                                    <input type="hidden" name="category" value="2">
                                    <input type="hidden" name="idpro" value="{{ $inmueble["idpro"] }}">
                                    <input type="hidden" name="codpro" value="{{ $inmueble["codpro"] }}">
                                    <input type="hidden" name="branch" value="{{ $inmueble["branch"] }}">

                                    <div class="form-group">
                                        <label>Nombre</label>
                                        <input type="text" class="form-control" name="name" required>
                                    </div><!-- /.form-group -->

                                    <div class="form-group">
                                        <label>Correo</label>
                                        <input type="email" class="form-control" name="email" required>
                                    </div><!-- /.form-group -->

                                    <div class="form-group">
                                        <label>Teléfono</label>
                                        <input type="text" class="form-control" name="phone">
                                    </div><!-- /.form-group -->

                                    <div class="form-group">
                                        <label>Asunto</label>
                                        <input type="text" class="form-control" name="subject" required>
                                    </div><!-- /.form-group -->

                                    <div class="form-group">
                                        <label>Mensaje</label>
                                        <textarea class="form-control" rows="4" name="message" required></textarea>
                                    </div><!-- /.form-group -->

                                    <div class="form-group-btn">
                                        <button type="submit" class="btn btn-primary btn-block">Enviar</button>
                                    </div><!-- /.form-group-btn -->

                                    <div class="form-group">
                                        <h3 class="color-primary enviando">Enviando ...</h3>
                                        <h3 class="color-primary correcto">Mensaje enviado Correctamente</h3>
                                    </div>
                                </form>
                            </div><!-- /.widget -->

                            @if ($inmueble["biz_code"] != 2)
                                <div class="widget widget-background-white">
                                    <h3 class="widgettitle">Requisitos para el arrendamiento</h3>

                                    <h5>Persona Natural</h5>

                                    <div class="row">

                                        <div class="col-md-12">
                                            <a href="/assets/docs/instrucciones_persona_natural.pdf" class="link-black" target="_blank">
                                                <img src="/assets/img/pdf.png" alt="" width="50px">
                                                Instrucciones
                                            </a>
                                        </div>

                                        <div class="col-md-12">
                                            <a href="/assets/docs/persona_natural.pdf" class="link-black" target="_blank">
                                                <img src="/assets/img/pdf.png" alt="" width="50px">
                                                Formulario
                                            </a>
                                        </div>

                                    </div>
                                    <br>

                                    <h5>Persona Jurídica</h5>

                                    <div class="row">

                                        <div class="col-md-12">
                                            <a href="/assets/docs/instrucciones_persona_juridica.pdf" class="link-black" target="_blank">
                                                <img src="/assets/img/pdf.png" alt="" width="50px">
                                                Instrucciones
                                            </a>
                                        </div>

                                        <div class="col-md-12">
                                            <a href="/assets/docs/persona_juridica.pdf" class="link-black" target="_blank">
                                                <img src="/assets/img/pdf.png" alt="" width="50px">
                                                Formulario
                                            </a>
                                        </div>
                                    </div>

                                    <br>
                                    <h5>Mapa</h5>

                                    <div class="row">
                                        <div class="col-md-12">
                                            <div id="map-detalle" style="height: 500px;"></div>
                                        </div>
                                    </div>

                                </div>
                            @endif

                        </div><!-- /.col-* -->
                    </div><!-- /.row -->
                </div><!-- /.container -->
            </div><!-- /.content -->
        </div><!-- /.main-inner -->
    </div><!-- /.main -->
</div><!-- /.main-wrapper -->


<div id="modalSendEmail" class="modal fade" role="dialog">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Enviar Inmueble por correo Electrónico</h4>
            </div>
            <div class="modal-body">
                <p>
                <form action="" id="formmail2" method="post">
                    <input type="hidden" name="category" value="3">
                    <input type="hidden" name="codpro" value="{{ $inmueble["codpro"] }}">
                    <input type="hidden" name="url" value="{{ $meta_url }}">
                    <input type="hidden" name="title" value="{{ $titulo }}">
                    <input type="hidden" name="description" value="{{ $descripcion }}">

                    <div class="form-group">
                        <label for="name">Nombre</label>
                        <input type="text" name="name" class="form-control" required>
                    </div>
                    <div class="form-group">
                        <label for="email">Su Correo</label>
                        <input type="email" name="email" class="form-control" required>
                    </div>

                    <div class="form-group">
                        <label for="sendmail">Correo a enviarlo</label>
                        <input type="email" name="sendmail" class="form-control" required>
                    </div>

                    <div class="form-group">
                        <label for="message">Mensaje</label>
                        <textarea name="message" class="form-control" required></textarea>
                    </div>

                    <div class="form-group">
                        <button type="submit" class="btn btn-primary pull-right">Enviar</button>
                    </div><!-- /.col-* -->

                    <div class="form-group">
                        <h3 class="color-primary enviando">Enviando ...</h3>
                        <h3 class="color-primary correcto">Mensaje enviado Correctamente</h3>
                    </div>

                    <br><br>

                </form>
                </p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
            </div>
        </div>

    </div>
</div>

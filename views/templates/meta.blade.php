<meta property="og:title" content="{{ $titulo }}" />
<meta property="og:description" content="{{ $descripcion }}" />
<meta property="og:url" content="{{ $meta_url }}" />
<meta property="og:image" content="{{ $meta_image }}" />
<meta property="og:image:width" content="{{ $width }}" />
<meta property="og:image:height" content="{{ $height }}" />

<div class="footer-wrapper">
    {{-- <div class="container">
        <div class="footer-inner">
            <div class="footer-top">
                <div class="footer-top-left">
                    <img src="/assets/img/header/logo_pie.png" alt="logo" class="img-footer">
                    <h2>UNIPROYECTOS S.A.S.</h2>

                    <p>
                        Calle 95 #11-51 Of. 402 - Bogotá D.C, Colombia <br>
                        Tel.: (57) (60+1) 616 7840<br>
                        Cel.: 311 228 4125 - 310 580 9772 <br>
                        Matrícula de Arrendador 1484
                    </p>
                </div>

                <div class="footer-top-right">
                    <div id="map-contact" class="contact-map footer-map">
                    </div>
                </div>
            </div>

            <div class="footer-bottom">
                <div class="footer-left">
                    <a href="/assets/docs/politica.pdf" target="_blank">Política de Tratamiento y protección de Datos</a> |
                    &copy; Powered by <a href="https://domus.la" target="_blank">Domus</a>.
                </div>

                <div class="footer-right">
                    <ul class="nav nav-pills">

                        <li class="nav-item"><a href="/contact" class="nav-link">Contacto</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div> --}}
</div>
</div>

<script type="text/javascript" src="/assets/js/jquery.js"></script>
<script type="text/javascript" src="/assets/js/jquery.ezmark.min.js"></script>
<script type="text/javascript" src="/assets/js/tether.min.js"></script>
<script type="text/javascript" src="/assets/js/bootstrap.min.js"></script>
<script type="text/javascript" src="/assets/js/gmap3.min.js"></script>
<script type="text/javascript" src="/assets/js/leaflet.js"></script>
<script type="text/javascript" src="/assets/js/leaflet.markercluster.js"></script>
{{-- Google Maps --}}
<script async src="https://maps.googleapis.com/maps/api/js?key=AIzaSyA_Y6SOJOGNTdgrnE4BdwOM1En7GABfzLM&libraries=drawing&v=weekly"></script>

<script type="text/javascript" src="/assets/js/owl.carousel.min.js"></script>
<script type="text/javascript" src="/assets/js/chartist.min.js"></script>
<script type="text/javascript" src="/assets/js/scrollPosStyler.js"></script>
<script type="text/javascript" src="/assets/js/villareal.js?v=1.0"></script>

<script src="https://cdnjs.cloudflare.com/ajax/libs/lightslider/1.1.6/js/lightslider.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/lightgallery/1.3.9/js/lightgallery.min.js"></script>

{{-- VUEJS --}}
<script src="//unpkg.com/vue@3/dist/vue.global.js"></script>
<script type="module" src="/assets/js/global.js"></script>

</body>

</html>

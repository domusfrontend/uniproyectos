<!DOCTYPE html>
<html lang="es" ng-app="app">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    @if (isset($detalle) && $detalle == 2)
        @include("templates.meta")
    @else
        <meta property="og:image" content="/assets/img/logo.png?V1.0">
        <meta property="og:title" content="{{ $titulo }} | Uniproyectos" />
        <meta property="og:description" content="Uniproyectos Inmobiliaria" />
        <meta property="og:image:width" content="500" />
        <meta property="og:image:height" content="500" />
    @endif

    <title>{{ $titulo }} - Uniproyectos</title>
    <link href="https://fonts.googleapis.com/css?family=Raleway:300,400,500,600,300,700&subset=latin,latin-ext" rel="stylesheet" type="text/css">
    <link href="/assets/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <link href="/assets/css/owl.carousel.css" rel="stylesheet" type="text/css">
    <link href="/assets/css/chartist.min.css" rel="stylesheet" type="text/css">
    <link href="/assets/css/leaflet.css" rel="stylesheet" type="text/css">
    <link href="/assets/css/leaflet.markercluster.css" rel="stylesheet" type="text/css">
    <link href="/assets/css/leaflet.markercluster.default.css" rel="stylesheet" type="text/css">
    <link href="/assets/css/villareal-turquoise.css" rel="stylesheet" type="text/css" id="css-primary">
    <!-- <link href="//netdna.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet"> -->
    <link rel="stylesheet" type="text/css" href="/assets/css/index.css?v=1.0">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/lightgallery/1.3.9/css/lightgallery.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/lightslider/1.1.6/css/lightslider.min.css">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.1.0/css/all.css" integrity="sha384-lKuwvrZot6UHsBSfcMvOkWwlCMgc0TaWr+30HWe3a4ltaBwTZhyTEggF5tJv8tbt" crossorigin="anonymous">

    <link rel="icon" href="/assets/img/favicon.png" type="image/x-icon">

    @if (isset($styles) && count($styles) > 0)
        @foreach ($styles as $style)
            <link rel="stylesheet" href="/assets/css/{{ $style }}.css">
        @endforeach
    @endif
</head>

<body>
    <div class="page-wrapper">

        @if (isset($detalle) && $detalle == 2)
            <a href="https://wa.me/573118208908?text=Buen día, escribo desde la página web, estoy interesado (a) en {{ $titulo }} {{ $codigo }}, en esta url {{ $meta_url }}" target="_blank" class="wap">
                <img src="/assets/img/wap.png" alt="Escríbenos" class="vatt">
            </a>
        @else
            <a href="https://wa.me/573118208908?text=Buen día, escribo desde la página web" target="_blank" class="wap">
                <img src="/assets/img/wap.png" alt="Escríbenos" class="vatt">
            </a>
        @endif

        <div class="header-wrapper">
            {{-- <div class="header">
                <div class="header-inner">
                    <div class="container">
                        <div class="header-top">
                            <div class="header-top-inner">
                                <a class="header-logo" href="/index.php">
                                    <img src="/assets/img/header/logo.png" alt="">
                                </a>

                                <div class="header-information">
                                    <i class="fa fa-at"></i>

                                    <div class="header-information-block">
                                        <strong>comercial@uniproyectos.com.co</strong>
                                        <span>Calle 95 #11-51 Of. 402 - Bogotá D.C, Colombia </span>
                                    </div>
                                </div>

                                <div class="header-information">
                                    <i class="fa fa-phone"></i>

                                    <div class="header-information-block">
                                        <strong class="text-right">(57) (60+1) 616 7840</strong>
                                        <span>(+571) 310 580 9772 - 311 820 8908</span>
                                    </div>
                                </div>

                                <button class="navbar-toggler pull-xs-right hidden-md-up" type="button" data-toggle="collapse" data-target=".nav-primary-wrapper">
                                    <span></span>
                                    <span></span>
                                    <span></span>
                                </button>
                            </div>
                        </div>

                        <div class="header-bottom">
                            <div class="header-bottom-inner">

                                <div class="nav-primary-wrapper collapse navbar-toggleable-sm">
                                    <ul class="nav nav-pills nav-primary" style="font-size: 15px;">
                                        <li class="nav-item">
                                            <a href="/" class="nav-link {{ $activeInicio ?? "" }}">
                                                Inicio
                                            </a>
                                        </li>
                                        <li class="nav-item">
                                            <a href="/about_us" class="nav-link {{ $activeNosotros ?? "" }}">
                                                Nosotros
                                            </a>
                                        </li>
                                        <li class="nav-item">
                                            <a href="/busqueda/gestion/arriendo" class="nav-link {{ $activeArriendo ?? "" }}">
                                                Arriendo
                                            </a>
                                        </li>
                                        <li class="nav-item">
                                            <a href="/busqueda/gestion/venta" class="nav-link {{ $activeVenta ?? "" }}">
                                                Venta
                                            </a>
                                        </li>
                                        <li class="nav-item">
                                            <a href="/mapa" class="nav-link {{ $activeMapa ?? "" }}">
                                                Búsqueda por Mapa
                                            </a>
                                        </li>
                                        <li class="nav-item">
                                            <a href="/contact" class="nav-link {{ $activeContacto ?? "" }}">
                                                Contacto
                                            </a>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div> --}}
        </div>

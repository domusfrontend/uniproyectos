<div class=""></div>
<div class="main-wrapper" id="content-page">
    <div class="main">
        <div class="main-inner">
            <div class="content">
                <div class="map-leaflet-wrapper pull-top">
                    <div id="slider-op" class="slider-index">
                        <!--Slider de imágenes del Index-->
                        <ul id="imageGallery1">
                            <li data-thumb="/assets/img/slider-1.jpg" style="max-width: 100%;">
                                <img src="/assets/img/slider-1.jpg" style="height: 500px; object-fit: cover;">
                            </li>
                            <li data-thumb="/assets/img/slider-2.jpg" style="max-width: 100%;">
                                <img src="/assets/img/slider-2.jpg" style="height: 500px; object-fit: cover;">
                            </li>
                            <li data-thumb="/assets/img/slider-3.jpg" style="max-width: 100%;">
                                <img src="/assets/img/slider-3.jpg" style="height: 500px; object-fit: cover;">
                            </li>
                        </ul>
                        <!--Final de Slider de imágenes del Index-->
                    </div>

                    <div class="map-filter-wrapper map-filter-horizontal">
                        <div class="map-filter filter">
                            <div class="row">
                                <div class="form-group col-md-2">
                                    <label>Tipo</label>

                                    <select class="form-control" id="tipofiltro">
                                        <option value="">Todos</option>
                                        @foreach ($tipos["data"] as $tipo)
                                            <option value="{{ $clase->replacespacios($tipo["name"]) }}">
                                                {{ $clase->cletter($tipo["name"]) }}
                                            </option>
                                        @endforeach
                                    </select>
                                </div><!-- /.form-group -->

                                <div class="form-group col-md-2">
                                    <label>Gestión</label>

                                    <select class="form-control" id="gestionfiltro">
                                        <option value="">Todas</option>
                                        <option value="arriendo">Arriendo</option>
                                        <option value="venta">Venta</option>
                                    </select>
                                </div><!-- /.form-group -->

                                <div class="form-group col-md-2">
                                    <label>Ciudad</label>

                                    <select class="form-control" id="ciudadfiltro">
                                        <option value="">Todas</option>
                                        @foreach ($ciudades["data"] as $ciudad)
                                            <option value="{{ $clase->replacespacios($ciudad["name"]) }}">
                                                {{ $clase->cletter($ciudad["name"]) }}
                                            </option>
                                        @endforeach
                                    </select>
                                </div><!-- /.form-group -->
                                <div class="form-group col-md-2">
                                    <label>Zona</label>
                                    
                                    <select class="form-control" id="zonafiltro">
                                        <option value="">Todas</option>
                                        @foreach ($zonas["data"] as $zona)
                                            <option value="{{ $clase->replacespacios($zona["name"]) }}">
                                                {{ $clase->cletter($zona["name"]) }}
                                            </option>
                                        @endforeach
                                    </select>
                                </div><!-- /.form-group -->
                                <div class="form-group col-md-2">
                                    <label>Código</label>
                                    <input type="text" class="form-control" id="codigofiltro" placeholder="Ej: 4953">
                                </div><!-- /.form-group -->
                                <div class="col-md-2">
                                    <div class="form-group-btn form-group-btn-placeholder-gap">
                                        <button class="btn btn-primary btn-block" @click.prevent="search(1)">Buscar</button>
                                    </div><!-- /.form-group -->
                                </div><!-- /.col-* -->
                            </div><!-- /.row -->
                        </div><!-- /.filter -->
                    </div><!-- /.filter-wrapper -->
                </div><!-- /.map-leaflet-wrapper -->

                <div class="information-bar">
                    <div class="container">
                        <p style="font-size: 25px;">
                            <i class="fa fa-star"></i> Consigne su inmueble con nosotros
                            <a href="/publish">Aquí</a>
                        </p>
                    </div><!-- /.container -->
                </div><!-- /.information-bar -->
                <div class="container margin-top">
                    <div class="row">
                        <div class="col-xs-12 col-md-4 pagoscontainer">
                            <div class="containerpro">
                                <i class="fa fa-users"></i>
                                <h3>Propietario</h3>
                                <p><a href="https://www.depias.com/" target="_blank">Estado de cuenta</a></p>
                            </div>
                        </div>
                        <div class="col-xs-12 col-md-4 pagoscontainer">
                            <div class="containerpro">
                                <a href="https://www.zonapagos.com/t_uniproyectos/pagos.asp" target="_blank">
                                    <img src="/assets/img/index/pse.png" alt=""></a>
                            </div>
                        </div>
                        <div class="col-xs-12 col-md-4 pagoscontainer">
                            <div class="containerpro">
                                <i class="fa fa-barcode"></i>
                                <h3>Arrendatario</h3>
                                <p><a href="https://www.depias.com/" target="_blank">Factura</a></p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="container">
                    <h2 class="center h1_directory">
                        <br>Encuentra lo que estás buscando. <br><br>
                    </h2>
                    <div class="row">

                        @if (isset($destacados[0]))
                            <div class="col-md-6">
                                <div class="listing-box listing-box-simple">
                                    <div class="listing-box-image" style="background-image: url('{{ $destacados[0]["image1"] }}')">
                                        <div class="listing-box-image-title">
                                            <h2>
                                                <a href="/inmuebles/{{ $clase->replacespacios($destacados[0]["type"]) }}-en-{{ $clase->replacespacios($destacados[0]["biz"]) }}-{{ $clase->replacespacios($destacados[0]["city"]) }}/{{ $destacados[0]["idpro"] }}/{{ $destacados[0]["codpro"] }}">
                                                    {{ $destacados[0]["type"] }}
                                                </a>
                                            </h2>
                                            <h3>
                                                {{ $destacados[0]["price_format"] }}
                                            </h3>
                                        </div>
                                        <span class="listing-box-image-links">
                                            <a href="/inmuebles/{{ $clase->replacespacios($destacados[0]["type"]) }}-en-{{ $clase->replacespacios($destacados[0]["biz"]) }}-{{ $clase->replacespacios($destacados[0]["city"]) }}/{{ $destacados[0]["idpro"] }}/{{ $destacados[0]["codpro"] }}">
                                                <i class="fa fa-search"></i>
                                                <span>Ver Detalle</span>
                                            </a>
                                        </span>
                                    </div>
                                </div>
                            </div>
                        @endif

                        <div class="col-md-6">
                            <div class="row">

                                @if (isset($destacados[1]))
                                    <div class="col-sm-6" ng-if="generalfactory.destacados[1]">
                                        <div class="listing-box">
                                            <div class="listing-box-image" style="background-image: url('{{ $destacados[1]["image1"] }}')">
                                                <div class="listing-box-image-label">Destacado</div><!-- /.listing-box-image-label -->
                                                <span class="listing-box-image-links">
                                                    <a href="/inmuebles/{{ $clase->replacespacios($destacados[1]["type"]) }}-en-{{ $clase->replacespacios($destacados[1]["biz"]) }}-{{ $clase->replacespacios($destacados[1]["city"]) }}/{{ $destacados[1]["idpro"] }}/{{ $destacados[1]["codpro"] }}">
                                                        <i class="fa fa-search"></i>
                                                        <span>Ver Detalle</span>
                                                    </a>
                                                </span>
                                            </div>
                                            <div class="listing-box-title">
                                                <h2>
                                                    <a href="/inmuebles/{{ $clase->replacespacios($destacados[1]["type"]) }}-en-{{ $clase->replacespacios($destacados[1]["biz"]) }}-{{ $clase->replacespacios($destacados[1]["city"]) }}/{{ $destacados[1]["idpro"] }}/{{ $destacados[1]["codpro"] }}">
                                                        {{ $destacados[1]["type"] }}
                                                    </a>
                                                </h2>
                                                <h3>
                                                    {{ $destacados[1]["price_format"] }}
                                                </h3>
                                            </div>
                                            <div class="listing-box-content">
                                                <dl>
                                                    <dt>{{ $clase->cletter($destacados[1]["neighborhood"]) }}</dt>
                                                    <dd></dd>
                                                    <dt><i class="fa fa-chart-area"></i> {{ $destacados[1]["area_cons"] }} m2</dt>
                                                    <dd><i class="fa fa-bed"></i> {{ $destacados[1]["bedrooms"] }}</dd>
                                                    <dt><i class="fa fa-car"></i> {{ $destacados[1]["parking"] }}</dt>
                                                    <dd><i class="fa fa-bath"></i> {{ $destacados[1]["bathrooms"] }}</dd>
                                                </dl>
                                            </div>
                                        </div>
                                    </div>
                                @endif

                                @if (isset($destacados[2]))
                                    <div class="col-sm-6">
                                        <div class="listing-box">
                                            <div class="listing-box-image" style="background-image: url('{{ $destacados[2]["image1"] }}')">
                                                <div class="listing-box-image-label">Destacado</div><!-- /.listing-box-image-label -->

                                                <span class="listing-box-image-links">
                                                    <a href="/inmuebles/{{ $clase->replacespacios($destacados[2]["type"]) }}-en-{{ $clase->replacespacios($destacados[2]["biz"]) }}-{{ $clase->replacespacios($destacados[2]["city"]) }}/{{ $destacados[2]["idpro"] }}/{{ $destacados[2]["codpro"] }}">
                                                        <i class="fa fa-search"></i>
                                                        <span>Ver Detalle</span>
                                                    </a>
                                                </span>
                                            </div><!-- /.listing-box-image -->

                                            <div class="listing-box-title">
                                                <h2>
                                                    <a href="/inmuebles/{{ $clase->replacespacios($destacados[2]["type"]) }}-en-{{ $clase->replacespacios($destacados[2]["biz"]) }}-{{ $clase->replacespacios($destacados[2]["city"]) }}/{{ $destacados[2]["idpro"] }}/{{ $destacados[2]["codpro"] }}">
                                                        {{ $destacados[2]["type"] }}
                                                    </a>
                                                </h2>
                                                <h3>
                                                    {{ $destacados[2]["price_format"] }}
                                                </h3>
                                            </div><!-- /.listing-box-title -->

                                            <div class="listing-box-content">
                                                <dl>
                                                    <dt>{{ $clase->cletter($destacados[2]["neighborhood"]) }}</dt>
                                                    <dd></dd>
                                                    <dt><i class="fa fa-chart-area"></i> {{ $destacados[2]["area_cons"] }} m2</dt>
                                                    <dd><i class="fa fa-bed"></i> {{ $destacados[2]["bedrooms"] }}</dd>
                                                    <dt><i class="fa fa-car"></i> {{ $destacados[2]["parking"] }}</dt>
                                                    <dd><i class="fa fa-bath"></i> {{ $destacados[2]["bathrooms"] }}</dd>
                                                </dl>
                                            </div><!-- /.listing-box-cotntent -->
                                        </div><!-- /.listing-box -->
                                    </div><!-- /.col-* -->
                                @endif
                            </div><!-- /.row -->
                        </div><!-- /.col-* -->
                    </div><!-- /.row -->

                    <div class="row">
                        <div class="col-md-12">
                            <div class="listing-box-wrapper">
                                <div class="row">

                                    @foreach ($destacados as $key => $inmueble)
                                        @if ($key > 2)
                                            <div class="col-md-3">
                                                <div class="listing-box">
                                                    <div class="listing-box-image" style="background-image: url('{{ $inmueble["image1"] }}')">
                                                        <div class="listing-box-image-label">Destacado</div><!-- /.listing-box-image-label -->

                                                        <span class="listing-box-image-links">
                                                            <a href="/inmuebles/{{ $clase->replacespacios($inmueble["type"]) }}-en-{{ $clase->replacespacios($inmueble["biz"]) }}-{{ $clase->replacespacios($inmueble["city"]) }}/{{ $inmueble["idpro"] }}/{{ $inmueble["codpro"] }}">
                                                                <i class="fa fa-search"></i>
                                                                <span>Ver Detalle</span>
                                                            </a>
                                                        </span>
                                                    </div><!-- /.listing-box-image -->

                                                    <div class="listing-box-title">
                                                        <h2>
                                                            <a href="/inmuebles/{{ $clase->replacespacios($inmueble["type"]) }}-en-{{ $clase->replacespacios($inmueble["biz"]) }}-{{ $clase->replacespacios($inmueble["city"]) }}/{{ $inmueble["idpro"] }}/{{ $inmueble["codpro"] }}">
                                                                {{ $inmueble["type"] }}
                                                            </a>
                                                        </h2>
                                                        <h3>
                                                            {{ $inmueble["price_format"] }}
                                                        </h3>
                                                    </div><!-- /.listing-box-title -->

                                                    <div class="listing-box-content">
                                                        <dl>
                                                            <dt>{{ $clase->cletter($inmueble["neighborhood"]) }}</dt>
                                                            <dd></dd>
                                                            <dt><i class="fa fa-chart-area"></i> {{ $inmueble["area_cons"] }} m2</dt>
                                                            <dd><i class="fa fa-bed"></i> {{ $inmueble["bedrooms"] }}</dd>
                                                            <dt><i class="fa fa-car"></i> {{ $inmueble["parking"] }}</dt>
                                                            <dd><i class="fa fa-bath"></i> {{ $inmueble["bathrooms"] }}</dd>
                                                        </dl>
                                                    </div><!-- /.listing-box-cotntent -->
                                                </div><!-- /.listing-box -->
                                            </div><!-- /.col-* -->
                                        @endif
                                    @endforeach

                                </div><!-- /.row -->
                            </div><!-- /.listing-box-wrapper -->
                        </div><!-- /.col-sm-* -->


                    </div><!-- /.row -->
                    <div class="row">
                        <div class="col-md-12">
                            <div class="listing-box-wrapper">
                                <div class="row">
                                    <h2 class="center h1_directory">
                                        <br>
                                        Directorio
                                    </h2>
                                    <div class="col-sm-4 col-xs-12 directory">
                                        <div class="title_directory col-sm-12">
                                            <h3>Ventas y Arrendamientos</h3>
                                        </div>
                                        <div class="container_directory col-sm-12">
                                            <div class="col-sm-10">
                                                <p><b>Telefono:</b> 310 580 9772 - 311 820 8908</p>

                                            </div>
                                            <div class="col-sm-2 link_container">
                                                <a @click="setEmail('comercial@uniproyectos.com.co')">
                                                    <i class="fas fa-external-link-alt"></i>
                                                </a>
                                            </div>
                                            <div class="col-sm-12" @click="setEmail('comercial@uniproyectos.com.co')" style="cursor:pointer">
                                                <p><b>Correo:</b> comercial@uniproyectos.com.co</p>
                                            </div>

                                        </div>

                                    </div>
                                    <div class="col-sm-4 col-xs-12 directory">
                                        <div class="title_directory col-sm-12">
                                            <h3>Reparaciones locativas</h3>
                                        </div>
                                        <div class="container_directory col-sm-12">
                                            <div class="col-sm-10">
                                                <p><b>Telefono:</b> 314 674 7034</p>

                                            </div>
                                            <div class="col-sm-2 link_container">
                                                <span @click="setEmail('locativas@uniproyectos.com.co')">
                                                    <i class="fas fa-external-link-alt"></i>
                                                </span>
                                            </div>
                                            <div class="col-sm-12" @click="setEmail('locativas@uniproyectos.com.co')" style="cursor:pointer">
                                                <p><b>Correo:</b> locativas@uniproyectos.com.co</p>
                                            </div>
                                        </div>

                                    </div>
                                    <div class="col-sm-4 col-xs-12 directory">
                                        <div class="title_directory col-sm-12">
                                            <h3>Administrativo y financiero</h3>
                                        </div>
                                        <div class="container_directory col-sm-12">
                                            <div class="col-sm-10">
                                                <p><b>Telefono:</b> 311 228 4125</p>

                                            </div>
                                            <div class="col-sm-2 link_container">
                                                <span @click="setEmail('administrativo@uniproyectos.com.co')">
                                                    <i class="fas fa-external-link-alt"></i>
                                                </span>
                                            </div>
                                            <div class="col-sm-12" @click="setEmail('administrativo@uniproyectos.com.co')" style="cursor:pointer">
                                                <p><b>Correo:</b> administrativo@uniproyectos.com.co</p>
                                            </div>
                                        </div>

                                    </div>

                                </div>
                                <div class="row" v-if="email != ''">
                                    <div class="col-md-12">
                                        <div class="page-subheader page-subheader-small">
                                            <h3>Formulario de contacto</h3>
                                        </div>

                                        <div class="row">
                                            <form method="post" action="" id="formmail2">

                                                <input type="hidden" name="category" value="4">

                                                <div class="col-sm-12">
                                                    <div class="form-group">
                                                        <label for="sendmail">Para</label>
                                                        <input type="hidden" name="sendmail" v-model="email">
                                                        <input type="text" class="form-control" v-model="email" disabled>
                                                    </div><!-- /.form-group -->
                                                </div><!-- /.col-* -->
                                                <div class="col-sm-6">
                                                    <div class="form-group">
                                                        <label for="name">Nombre *</label>
                                                        <input type="text" name="name" class="form-control">
                                                    </div><!-- /.form-group -->
                                                </div><!-- /.col-* -->

                                                <div class="col-sm-6">
                                                    <div class="form-group">
                                                        <label for="email">Correo *</label>
                                                        <input type="mail" name="email" class="form-control">
                                                    </div><!-- /.form-group -->
                                                </div><!-- /.col-* -->

                                                <div class="col-sm-12">
                                                    <div class="form-group">
                                                        <label>Mensaje *</label>
                                                        <textarea class="form-control" name="message" rows="6"></textarea>
                                                    </div><!-- /.form-control -->
                                                </div><!-- /.col-* -->

                                                <div class="col-sm-12">
                                                    <button type="submit" class="btn btn-primary pull-right">Enviar</button>
                                                </div><!-- /.col-* -->

                                                <div class="form-group">
                                                    <h3 class="color-primary enviando">Enviando ...</h3>
                                                    <h3 class="color-primary correcto">Mensaje enviado Correctamente</h3>
                                                </div>

                                                <br><br>
                                            </form>
                                        </div><!-- /.row -->
                                    </div><!-- /.col-* -->
                                </div>
                            </div>
                        </div>
                    </div>
                </div><!-- /.container -->

                <div class="container">
                    <h2 class="center h1_directory">
                        <br>
                        Nuestros Aliados
                        <br>
                    </h2>

                    <div class="listing-box-wrapper listing-box-background">
                        <div class="listing-carousel-wrapper">
                            <div class="listing-carousel">
                                <div class="imgparner">
                                    <a href="http://ligainmobiliaria.co/" target="_blank">
                                        <img src="/assets/img/index/liga.png" alt="parner">
                                    </a>
                                </div>
                                <div class="imgparner">
                                    <a href="https://www.ciencuadras.com/" target="_blank">
                                        <img src="/assets/img/index/ciencuadras.png" alt="parner">
                                    </a>
                                </div>
                                <div class="imgparner">
                                    <a href="https://www.fincaraiz.com.co/" target="_blank">
                                        <img src="/assets/img/index/fincaraiz.png" alt="parner">
                                    </a>
                                </div>
                                <div class="imgparner">
                                    <a href="https://www.metrocuadrado.com/" target="_blank">
                                        <img src="/assets/img/index/metrocuadrado.png" alt="parner">
                                    </a>
                                </div>
                                <div class="imgparner">
                                    <a href="http://www.afydi.com/" target="_blank">
                                        <img src="/assets/img/index/afydi.png?v=1.0" alt="parner" class="afydiLogo">
                                    </a>
                                </div>
                                <div class="imgparner">
                                    <a href="http://www.ellibertador.co" target="_blank">
                                        <img src="/assets/img/index/libertador.png" alt="parner">
                                    </a>
                                </div>
                                <div class="imgparner">
                                    <a href="http://www.segurosbolivar.com.co" target="_blank">
                                        <img src="/assets/img/index/bolivar.png" alt="parner">
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div><!-- /.container -->
            </div><!-- /.content -->
        </div><!-- /.main-inner -->
    </div><!-- /.main -->
</div><!-- /.main-wrapper -->

<div id="content-page">
    <div id="carga">
        <img src="/assets/img/iconos/Carga.gif" />
    </div>
    <div class=" widdle">
        <div class=" All perfect">
            <div class="col-md-12" style="background-color: #c5c5c5;height: 73vh; padding: 0;">
                <div id="floating-panel">
                    <input id="draw-map" type="button" value="Dibujar" />
                    <input id="clear-map" type="button" value="Limpiar" />
                </div>
                <div id="map" class="altomapa"></div>
            </div>
        </div>
    </div>
</div>
<!--modal test-->

<!-- Modal -->
<div id="myModal" class="modal fade" role="dialog">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">¿Cómo utilizar la Búsqueda por Mapa?</h4>
            </div>
            <div class="modal-body">
                <p>
                    <video controls loop class="embed-responsive-item" width="100%">
                        <source src="/assets/video/tutorial.mp4" type="video/mp4" />
                    </video>
                </p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
            </div>
        </div>

    </div>
</div>

<div class="main-wrapper" id="content-page">
    <div class="main">
        <div class="main-inner">

            <div class="content-title">
                <div class="content-title-inner">
                    <div class="container">
                        <h1>
                            Lista de inmuebles
                            @if (isset($gestion) && $gestion == 1)
                                en Arriendo
                            @elseif (isset($gestion) && $gestion == 1)
                                en Venta
                            @endif
                        </h1>

                        <ol class="breadcrumb">
                            <li><a href="/index.php">Inicio</a></li>
                            <li><a href="/list.php">Propiedades</a></li>
                        </ol>
                    </div><!-- /.container -->
                </div><!-- /.content-title-inner -->
            </div><!-- /.content-title -->


            <div class="content">
                <div class="container">
                    <div class="filter filter-gray push-bottom">
                        <form method="get" action="?">

                            <div class="row filter-options">
                                <div class="col-sm-6">
                                    <div class="filter-sort">
                                        <strong>Ordenar por</strong>

                                        <div class="checkbox-inline">
                                            <label>
                                                <input ng-model="generalfactory.filters.orden" type="radio" name="filter-form-order" value="preciomin">
                                                Precio Minimo
                                            </label>
                                        </div>

                                        <div class="checkbox-inline">
                                            <label>
                                                <input ng-model="generalfactory.filters.orden" type="radio" name="filter-form-order" value="preciomax">
                                                Precio Maximo
                                            </label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>

                    <div class="row">
                        <div class="col-md-8 col-lg-9">

                            @foreach ($inmuebles["data"] as $inmueble)
                                <div class="listing-row">
                                    <div class="listing-row-inner">
                                        <div class="listing-row-image" style="background-image: url('{{ $inmueble["image1"] }}');">
                                            <span class="listing-row-image-links">
                                                <a href="/inmuebles/{{ $clase->replacespacios($inmueble["type"]) }}-en-{{ $clase->replacespacios($inmueble["biz"]) }}-{{ $clase->replacespacios($inmueble["city"]) }}/{{ $inmueble["idpro"] }}/{{ $inmueble["codpro"] }}">
                                                    <i class="fa fa-search"></i>
                                                    <span>Ver detalle</span>
                                                </a>
                                            </span>
                                        </div>

                                        <div class="listing-row-content">
                                            <h3>
                                                <a href="/inmuebles/{{ $clase->replacespacios($inmueble["type"]) }}-en-{{ $clase->replacespacios($inmueble["biz"]) }}-{{ $clase->replacespacios($inmueble["city"]) }}/{{ $inmueble["idpro"] }}/{{ $inmueble["codpro"] }}">
                                                    {{ $clase->cletter($inmueble["type"]) }} en
                                                    {{ $clase->cletter($inmueble["neighborhood"]) }}
                                                </a>
                                            </h3>
                                            <h4>{{ $inmueble["price_format"] }}</h4>

                                            <ul class="listing-row-attributes">
                                                <li>
                                                    <strong><i class="fa fa-map-marker"></i> Ciudad</strong>
                                                    <span>{{ $clase->cletter($inmueble["city"]) }}</span>
                                                </li>

                                                <li>
                                                    <strong><i class="fa fa-building"></i> Tipo</strong>
                                                    <span>{{ $clase->cletter($inmueble["type"]) }}</span>
                                                </li>

                                                <li>
                                                    <strong><i class="fa fa-certificate"></i> Gestión</strong>
                                                    <span>{{ $clase->cletter($inmueble["biz"]) }}</span>
                                                </li>

                                                <li>
                                                    <strong><i class="fa fa-arrows-alt"></i> Area</strong>
                                                    <span>{{ $inmueble["area_cons"] }} M2</span>
                                                </li>

                                                <li>
                                                    <strong><i class="fa fa-umbrella"></i> Baños</strong>
                                                    <span>{{ $inmueble["bathrooms"] }}</span>
                                                </li>

                                                <li>
                                                    <strong><i class="fa fa-bed"></i> Habitaciones</strong>
                                                    <span>{{ $inmueble["bedrooms"] }}</span>
                                                </li>
                                            </ul>
                                        </div><!-- /.listing-row-content -->
                                    </div><!-- /.listing-row-inner -->
                                </div><!-- /.listing-row -->
                            @endforeach

                            <div class="pagination-wrapper">
                                <ul class="pagination {{ $inmuebles["total"] == 0 ? "hide" : "" }}">
                                    @if ($inmuebles["current_page"] > 1)
                                        <li class="page-item">
                                            <a @click="search('{{ $inmuebles["current_page"] - 1 }}')" class="page-link active pointer" aria-label="Previous">
                                                <span aria-hidden="true">«</span>
                                                <span class="sr-only">Anterior</span>
                                            </a>
                                        </li>
                                    @endif

                                    @if ($inmuebles["current_page"] - 2 >= 1)
                                        <li class="page-item">
                                            <a @click="search('{{ $inmuebles["current_page"] - 2 }}')" class="page-link pointer">
                                                {{ $inmuebles["current_page"] - 2 }}
                                            </a>
                                        </li>
                                    @endif

                                    @if ($inmuebles["current_page"] > 1)
                                        <li class="page-item">
                                            <a @click="search('{{ $inmuebles["current_page"] - 1 }}')" class="page-link pointer">
                                                {{ $inmuebles["current_page"] - 1 }}
                                            </a>
                                        </li>
                                    @endif

                                    <li class="page-item">
                                        <a class="page-link active">
                                            {{ $inmuebles["current_page"] }}
                                        </a>
                                    </li>

                                    @if ($inmuebles["current_page"] < $inmuebles["last_page"])
                                        <li class="page-item">
                                            <a @click="search('{{ $inmuebles["current_page"] + 1 }}')" class="page-link pointer">
                                                {{ $inmuebles["current_page"] + 1 }}
                                            </a>
                                        </li>
                                    @endif

                                    @if ($inmuebles["current_page"] + 2 <= $inmuebles["last_page"])
                                        <li class="page-item">
                                            <a @click="search('{{ $inmuebles["current_page"] + 2 }}')" class="page-link pointer">
                                                {{ $inmuebles["current_page"] + 2 }}
                                            </a>
                                        </li>
                                    @endif

                                    @if ($inmuebles["current_page"] != $inmuebles["last_page"])
                                        <li class="page-item">
                                            <a @click="search('{{ $inmuebles["current_page"] + 1 }}')" class="page-link active pointer" aria-label="Next">
                                                <span aria-hidden="true">»</span>
                                                <span class="sr-only">Siguiente</span>
                                            </a>
                                        </li>
                                    @endif
                                </ul><!-- /.pagination -->
                            </div><!-- /.pagination-wrapper -->
                        </div><!-- /.col-sm-* -->

                        <div class="col-md-4 col-lg-3">
                            <div class="widget" ng-init="GetTipos();GetGestiones();GetCiudades();GetZona()">
                                <h3 class="widgettitle">Filtros</h3>

                                <div class="row">
                                    <div class="form-group col-md-12">
                                        <label>Gestión</label>

                                        <select class="form-control" id="gestionfiltro">
                                            <option value="">Todas</option>
                                            <option value="arriendo" {{ isset($gestion) && $gestion == 1 ? "selected" : "" }}>Arriendo</option>
                                            <option value="venta" {{ isset($gestion) && $gestion == 2 ? "selected" : "" }}>Venta</option>
                                        </select>
                                    </div><!-- /.form-group -->

                                    <div class="form-group col-md-12">
                                        <label>Ciudad</label>

                                        <select class="form-control" id="ciudadfiltro">
                                            <option value="">Todas</option>
                                            @foreach ($ciudades["data"] as $ciudad)
                                                <option value="{{ $clase->replacespacios($ciudad["name"]) }}" {{ isset($ciudads) && $ciudads == $ciudad["code"] ? "selected" : "" }}>
                                                    {{ $clase->cletter($ciudad["name"]) }}
                                                </option>
                                            @endforeach
                                        </select>
                                    </div><!-- /.form-group -->
                                    <div class="form-group col-md-12">
                                        <label>Zona</label>

                                        <select class="form-control" id="zonafiltro">
                                            <option value="">Todas</option>
                                            @foreach ($zonas["data"] as $zona)
                                                <option value="{{ $clase->replacespacios($zona["name"]) }}" {{ isset($zonass) && $zonass == $zona["code"] ? "selected" : "" }}>
                                                    {{ $clase->cletter($zona["name"]) }}
                                                </option>
                                            @endforeach
                                        </select>
                                    </div><!-- /.form-group -->
                                    <div class="form-group col-md-12">
                                        <label>Tipo</label>

                                        <select class="form-control" id="tipofiltro">
                                            <option value="">Todos</option>
                                            @foreach ($tipos["data"] as $tipo)
                                                <option value="{{ $clase->replacespacios($tipo["name"]) }}" {{ isset($tiposs) && $tiposs == $tipo["code"] ? "selected" : "" }}>
                                                    {{ $clase->cletter($tipo["name"]) }}
                                                </option>
                                            @endforeach
                                        </select>
                                    </div><!-- /.form-group -->
                                    <div class="form-group col-md-12">
                                        <label>Barrio</label>
                                        <input type="text" class="form-control" id="barriofiltro" value="{{ $barrio }}" placeholder="Ej: Rosales">
                                    </div><!-- /.form-group -->

                                    <div class="col-md-12">
                                        <div class="row">
                                            <div class="col-sm-12">
                                                <label>Precio</label>
                                            </div>
                                            <div class="col-sm-6">
                                                <input type="text" class="form-control price_input" id="preciominfiltro" value="{{ $preciomin }}" placeholder="Mínimo">
                                            </div>
                                            <div class="col-sm-6">
                                                <input type="text" class="form-control price_input" id="preciomaxfiltro" value="{{ $preciomax }}" placeholder="Máximo">
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-md-12">
                                        <div class="row">
                                            <div class="col-sm-12">
                                                <label>Área</label>
                                            </div>
                                            <div class="col-sm-6">
                                                <input type="text" class="form-control" id="areaminfiltro" value="{{ $areamin }}" placeholder="Mínimo">
                                            </div>
                                            <div class="col-sm-6">
                                                <input type="text" class="form-control" id="areamaxfiltro" value="{{ $areamax }}" placeholder="Máximo">
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-md-12">
                                        <div class="row">
                                            <div class="col-sm-12">
                                                <label>Habitaciones</label>
                                            </div>
                                            <div class="col-sm-6">
                                                <input type="text" class="form-control" id="minhabitacionesfiltro" value="{{ $minhabitaciones }}" placeholder="Mínimo">
                                            </div>
                                            <div class="col-sm-6">
                                                <input type="text" class="form-control" id="maxhabitacionesfiltro" value="{{ $maxhabitaciones }}" placeholder="Máximo">
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-md-12">
                                        <div class="row">
                                            <div class="col-sm-12">
                                                <label>Baños</label>
                                            </div>
                                            <div class="col-sm-6">
                                                <input type="text" class="form-control" id="minbanosfiltro" value="{{ $minbanos }}" placeholder="Mínimo">
                                            </div>
                                            <div class="col-sm-6">
                                                <input type="text" class="form-control" id="maxbanosfiltro" value="{{ $maxbanos }}" placeholder="Máximo">
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-md-12">
                                        <div class="row">
                                            <div class="col-sm-12">
                                                <label>Parqueaderos</label>
                                            </div>
                                            <div class="col-sm-6">
                                                <input type="text" class="form-control" id="minparkeafiltro" value="{{ $minparkea }}" placeholder="Mínimo">
                                            </div>
                                            <div class="col-sm-6">
                                                <input type="text" class="form-control" id="maxparkeafiltro" value="{{ $maxparkea }}" placeholder="Máximo">
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-md-12">
                                        <div class="form-group-btn form-group-btn-placeholder-gap">
                                            <button class="btn btn-primary btn-block" @click.prevent="search(1)">Buscar</button>
                                        </div><!-- /.form-group -->
                                    </div><!-- /.col-* -->

                                </div><!-- /.row -->

                            </div><!-- /.widget -->
                        </div><!-- /.col-* -->
                    </div><!-- /.row -->
                </div><!-- /.container -->
            </div><!-- /.content -->
        </div><!-- /.main-inner -->
    </div><!-- /.main -->
</div><!-- /.main-wrapper -->
